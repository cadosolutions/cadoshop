from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from mptt.admin import MPTTModelAdmin

from . import models
from imagekit.admin import AdminThumbnail
from plata.shop.models import TaxClass
from django.conf import settings
from django import forms
from models import Product, ProductOption, ProductAttachment

from django.forms.models import inlineformset_factory
from django.contrib.admin.util import flatten_fieldsets
from django.utils.functional import curry
from cadocms.admin import StaticPageAdmin, Setting, SettingAdmin, ChunkAdmin
from cadocms.widgets import UrlOrFileInput


class ProductForm(forms.ModelForm):

    class Meta:
        model = Product
        widgets = {
            'image1': UrlOrFileInput(),
            'image2': UrlOrFileInput(),
            'image3': UrlOrFileInput(),
            'image4': UrlOrFileInput(),
            'image5': UrlOrFileInput(),
        }

    #def clean_slug(self):
        #data = self.cleaned_data['recipients']
        #if "fred@example.com" not in data:
        #    raise forms.ValidationError("You have forgotten about Fred!")

        # Always return the cleaned data, whether you have changed it or
        # not.
        #return data

class ProductAttachmentInline(admin.TabularInline): #admin.StackedInline):
    model = ProductAttachment
    extra = 1

class ProductOptionInline(admin.TabularInline):
    model = ProductOption
    extra = 0
    
    fieldsets = (
        (
         None, 
         {
            'fields': ('name', 'price_mod',  'colors', 'image')#'extra',
            }
        ),
    )    
    def __init__(self, parent_model, admin_site):
        if settings.CADOSHOP_MANAGESTOCK and 'items_in_stock' not in self.fieldsets[0][1]['fields']:
            self.fieldsets[0][1]['fields'] = self.fieldsets[0][1]['fields'] + ('items_in_stock',)
        super(admin.TabularInline, self).__init__(parent_model, admin_site)
        
    def get_formset(self, request, obj=None, **kwargs):
        return super(ProductOptionInline, self).get_formset(request, obj=obj, **kwargs)

class ProductAdmin(admin.ModelAdmin):
    form = ProductForm
    class Media:
        js = (
                #"/static/admin/js/inlines.js",  #this needs to be loaded before productadmin
                "/static/cadoshop/productadmin.js",
            )
    list_display = ('name', 'admin_thumbnail', 'category', 'code', 'get_price_string', 'is_active')
    list_display_links = ('name', 'admin_thumbnail',)
    list_filter = ('is_active', 'category', 'manufacturer', 'supplier')
    prepopulated_fields = {'slug': ('name', 'code')}
    search_fields = ('name', 'description', 'code')
    admin_thumbnail = AdminThumbnail(image_field='tiny_thumbnail')
    fieldsets = (
        (None, {
            'fields': ('name', 'code', 'slug', 'category', 'manufacturer', 'supplier', '_unit_price', 'description', 'is_active', 'colors')
        }),
        ('Fits to Products', {
            'classes': ('collapse', 'grp-collapse', 'grp-closed'),
            'fields': ('fits_to',)
        }),
        ('Extra Fields', {
            'classes': ('collapse', 'grp-collapse', 'grp-closed'),
            'fields': ('extra',)
        }),
        ('Images', {
            'classes': ('collapse', 'grp-collapse', 'grp-closed'),
            'fields': ('image1','image2','image3','image4','image5')
        }),
        ('Advanced options', {
            'classes': ('collapse', 'grp-collapse', 'grp-closed'),
            'fields': ('currency', 'tax_included', 'tax_class', 'forced_order')
        }),
    )
    radio_fields = {'tax_class': admin.HORIZONTAL, 'currency': admin.HORIZONTAL}
    
    inlines = (ProductAttachmentInline, ProductOptionInline, )
    
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "tax_class":
            kwargs["initial"] = TaxClass.objects.all()[0].id
        return super(ProductAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
    
    def formfield_for_choice_field(self, db_field, request, **kwargs):
        if db_field.name == "currency":
            kwargs["initial"] = settings.CURRENCIES[0]
        return super(ProductAdmin, self).formfield_for_choice_field(db_field, request, **kwargs)
    
    def save_form(self, request, form, change):
        #print form
        #print 'ASDASD'
        return form.save(commit=False)
    
    def save_model(self, request, obj, form, change):
        #print obj.values()
        obj.save()
        
    def save_formset(self, request, form, formset, change):
        #print 'FS'
        formset.save()

class ProductCategoryAdmin(MPTTModelAdmin):
    
    admin_thumbnail = AdminThumbnail(image_field='thumbnail')
    
    list_display = ('name', 'admin_thumbnail', 'get_excerpt', 'active')
    list_display_links = ('name',)
    list_filter = ('active', 'parent', 'level')
    
    search_fields = ('name', 'slug', 'description')
    prepopulated_fields = {'slug': ('name',)}
    fieldsets = (
        (None, {
            'fields': ('name', 'slug', 'parent', 'order', 'active', 'description', 'thumbnail', 'extra_search_terms')
        }),
        ('Advanced', {
            'fields': ('per_product_info', 'product_unit', 'product_default_count',)
        }),
        ('SEO', {
            'fields': ('seo_keywords', 'seo_description', 'seo_title',)
        }),
        ('Extra Fields', {
            'fields': ('extra_fields',)
        }),
    )
        
class ManufacturerAdmin(admin.ModelAdmin):
    admin_thumbnail = AdminThumbnail(image_field='tiny_thumbnail')
    list_display = ('name', 'admin_thumbnail', 'url')
    list_display_links = ('name',)
    
    prepopulated_fields = {'slug': ('name',)}
    fieldsets = (
        (None, {
            'fields': ('name', 'slug', 'logo', 'url' )
        }),
    )

class SupplierAdmin(admin.ModelAdmin):
    admin_thumbnail = AdminThumbnail(image_field='tiny_thumbnail')
    list_display = ('name',)
    list_display_links = ('name',)
    
    prepopulated_fields = {'slug': ('name',)}
    fieldsets = (
        (None, {
            'fields': ('name', 'slug' )
        }),
    )

from plata.shop.models import Order, OrderItem
from plata.shop.admin import OrderAdmin, OrderItemInline, AppliedDiscountInline, OrderStatusInline

admin.site.unregister(Order);

class CustomOrderItemInline(OrderItemInline):
    fieldsets = (
        (None, {'fields': ('product', 'show_thumbnail', 'show_code', 'quantity', 'currency', 'show_unit_price')}), #'_unit_tax', 'tax_rate', '_line_item_price', '_line_item_tax'
        )
    readonly_fields = ('product', 'show_thumbnail', 'show_code','quantity', 'currency', 'show_unit_price') #'_unit_tax', 'tax_rate', '_line_item_price', '_line_item_tax'
    
    def show_thumbnail(self, instance):
        return instance.product.get_tiny_thumbnail_img()
    show_thumbnail.short_description = 'Thumbnail'

    def show_unit_price(self, instance):
        if instance._unit_price:
            return '%.2f' % instance._unit_price

    def show_code(self, instance):
        if instance.product:
            if instance.product.product.code != None:
                return instance.product.product.code
            else:
                return ''
    show_code.short_description = 'Code'
    

class CustomOrderAdmin(OrderAdmin):
    inlines = [ CustomOrderItemInline, OrderStatusInline] #, AppliedDiscountInline
    
    
class ContactUsAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'message')
    fieldsets = [
        (None, {'fields':()}), 
        ]

    def __init__(self, *args, **kwargs):
        super(ContactUsAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = (None, )
    

admin.site.register(Order, CustomOrderAdmin)

admin.site.register(models.Manufacturer, ManufacturerAdmin)
admin.site.register(models.Supplier, SupplierAdmin)
admin.site.register(models.Product, ProductAdmin)
admin.site.register(models.ProductCategory, ProductCategoryAdmin)

admin.site.register(models.StaticPage, StaticPageAdmin)
admin.site.register(models.Chunk, ChunkAdmin)
admin.site.register(Setting, SettingAdmin)

admin.site.register(models.ContactUs, ContactUsAdmin)
