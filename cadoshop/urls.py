from django.conf.urls import include, patterns, url
from django.conf.urls.i18n import i18n_patterns

from views import shop

urlpatterns = i18n_patterns('',
    url(r'', include(shop.urls)),
    url(r'^$', 'cadoshop.views.index'),
    url(r'^products/(?P<category_slug>[A-Za-z0-9\-\_]*)$', 'cadoshop.views.search'),
    url(r'^product/(?P<product_slug>[A-Za-z0-9\-\_]*)/$', 'cadoshop.views.details'),
    url(r'^contact/$', 'cadoshop.views.contact'),
    url(r'^extrafields/(?P<category_id>\d+)$', 'cadoshop.views.extrafields', name='extrafields'),
    url(r'^reporting/', include('plata.reporting.urls')),
    
    url(r'^login/$', 'cadoshop.views.view_login', name='login'),
    url(r'^logout/$', 'cadoshop.views.view_logout', name='logout'),
                
    url(r'^register/$', 'cadoshop.views.view_register', name='register'),

    url(r'^password_reset/$', 'django.contrib.auth.views.password_reset', name='password_reset'),
    url(r'^password_reset/done/$', 'django.contrib.auth.views.password_reset_done', name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            'django.contrib.auth.views.password_reset_confirm', name='password_reset_confirm'),
    url(r'^reset/done/$', 'django.contrib.auth.views.password_reset_complete', name='password_reset_complete'),

 )