from plata.shop.processors import ProcessorBase
from plata.shop.models import TaxClass
import plata

class AdminEditableShippingProcessor(ProcessorBase):

    def process(self, order, items):
        #print 'PROCESSING!!!'
        if order.shipping_cost:
            cost = order.shipping_cost + order.shipping_tax
        else:
            cost = 0
            
        tax = TaxClass.objects.all()[0].rate

        order.shipping_cost, __ = self.split_cost(cost, tax)
        order.shipping_discount = min(
            order.discount_remaining,
            order.shipping_cost,
            )
        order.shipping_tax = tax / 100 * (
            order.shipping_cost - order.shipping_discount)

        self.set_processor_value('total', 'shipping',
            order.shipping_cost - order.shipping_discount
            + order.shipping_tax)

        tax_details = dict(order.data.get('tax_details', []))
        self.add_tax_details(tax_details, tax, order.shipping_cost,
            order.shipping_discount, order.shipping_tax)
        order.data['tax_details'] = tax_details.items()
