from cadocms.settings import BaseSettings
#import inspect, os

class Settings(object):
    
    CADO_NAME = 'Cado Shop'
    
    PLATA_REPORTING_STATIONERY = 'cadoshop.pdfstationery.Stationery'

    @property
    def INSTALLED_APPS(self):
        ret = super(Settings, self).INSTALLED_APPS;
        ret = list(ret)
        #after app before cadocms
        ret.insert(1, 'cadoshop')
        ret.insert(3, 'plata.discount')
        ret.insert(3, 'plata.contact')
        ret.insert(3, 'plata.shop')
        ret.insert(3, 'plata.payment')
        
        if self.CADOSHOP_MANAGESTOCK:
            ret.insert(3, 'plata.product.stock')
        
        #print ret
        return tuple(ret)

    CADO_EXTRA_ADMIN_LINKS = [
                {
                    'title': 'Rebuild Search Index',
                    'url': '/admin/rebuildindex/',
                    'external': False,
                },
            ]
    
    PLATA_SHOP_PRODUCT = 'cadoshop.ProductOption'
            
    GRAPPELLI_INDEX_DASHBOARD = 'cadoshop.dashboard.CustomIndexDashboard'
    
    POSTFINANCE = {
        'PSPID': 'plataTEST',
        'SHA1_IN': 'plataSHA1_IN',
        'SHA1_OUT': 'plataSHA1_OUT',
        'LIVE': False,
        }
    
    PAYPAL = {
        'BUSINESS': 'example@paypal.com',
        'LIVE': False,
        }
    
    
    PLATA_PAYMENT_MODULES = [
                'plata.payment.modules.paypal.PaymentProcessor',
                #'plata.payment.modules.cod.PaymentProcessor',
                'cadoshop.payment.bt.PaymentProcessor',
                'cadoshop.payment.contact.PaymentProcessor',
                'cadoshop.payment.cash.PaymentProcessor',
                ]
    
    PLATA_ORDER_PROCESSORS = [
        'plata.shop.processors.InitializeOrderProcessor',
        'plata.shop.processors.DiscountProcessor',
        'plata.shop.processors.TaxProcessor',
        'plata.shop.processors.MeansOfPaymentDiscountProcessor',
        'plata.shop.processors.ItemSummationProcessor',
        #'plata.shop.processors.ZeroShippingProcessor',
        'cadoshop.processors.AdminEditableShippingProcessor',
        'plata.shop.processors.OrderSummationProcessor',
    ]
    
    INDEX_STATIC_PAGE = None

    #one of 'none' 'yes' 'fancybox'
    CADOSHOP_PRODUCTPAGE = 'none'
    CADOSHOP_MANAGESTOCK = False
    CADOSHOP_ONLYSINGLE = False
    CADOSHOP_SEARCHABLEMANUFACTURERS = True
    CADOSHOP_ALLOWREGISTERING = False
    CADOSHOP_INDEXURL = "/products/"

    @property
    def PLATA_STOCK_TRACKING(self):
        return self.CADOSHOP_MANAGESTOCK
    
    CURRENCIES = ('PLN',)
    
    @property
    def EXTRA_URLCONFS(self):
        return super(Settings, self).EXTRA_URLCONFS + ['cadoshop.urls']


    PLATA_REPORTING_ADDRESSLINE = 'Example Corp. - 3. Example Street - 1234 Example'

    HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'
    
    @property
    def TEMPLATE_CONTEXT_PROCESSORS(self):
        return super(Settings, self).TEMPLATE_CONTEXT_PROCESSORS + (
               'cadoshop.views.frontend_context',
            )
    
    @property
    def HAYSTACK_CONNECTIONS(self):
        if self.HOST.CLASS != 'DEV':
            return {
                'default': {
                            'ENGINE': 'cadoshop.search.solr_grouping_backend.GroupedSolrEngine',
                            'URL': self.SOLR_CORE_URL,
                            },
                }
        else:
            return {
                'default': {
                            'ENGINE': 'cadoshop.search.dummy_grouping_backend.GroupedDummyEngine',
                    },
                }
    
    #TODO!!!!!
    #LOCALE_PATHS = (
    #    os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) + '/locale',
    #)