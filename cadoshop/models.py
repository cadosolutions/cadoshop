import sys

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.forms import fields
from django.core.exceptions import ValidationError
from cadocms.models import StaticPage as StaticPageBase
from cadocms.models import Chunk as ChunkBase
from django.conf import settings

from plata.product.models import ProductBase
from plata.shop.models import PriceBase

from cadocms.models import Tree, RootedTree, Sluggable, TreeForeignKey, ExtraFieldsUser, ExtraFieldsProvider
from cadocms.fields import ExtraFieldsDefinition, ExtraFieldsValues, HTMLField
 
from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import ResizeToFit, Adjust
from fields import ColorsField
from haystack import indexes
from django.core.urlresolvers import reverse

class StaticPage(StaticPageBase):
    #class Meta:
    #    app_label = settings.CADO_PROJECT
    pass

class Chunk(ChunkBase):
    #class Meta:
    #    app_label = settings.CADO_PROJECT
    pass


class ProductCategory(RootedTree, ExtraFieldsProvider, Sluggable):

    name = models.CharField(max_length=256, verbose_name=_('name'))
    active = models.BooleanField(default=True, verbose_name=_('active'))
    
    extra_search_terms = models.CharField(max_length=256, verbose_name=_('Extra search keywords'), blank=True, null=True)

    thumbnail = ProcessedImageField([ResizeToFit(50, 50, mat_color=(255, 255, 255))], upload_to='categories', format='JPEG', options={'quality': 90}, blank=True)
    description = models.TextField(blank=True, null=True)

    per_product_info = models.TextField(blank=True, null=True, help_text="this bit of text will appear on each product page")
    product_unit = models.CharField(max_length=128, blank=False, null=False, default="pcs")
    product_default_count = models.IntegerField(blank=False, null=False, default=1)

    seo_title = models.CharField(max_length=512, blank=True)
    seo_keywords = models.CharField(max_length=512, blank=True,
        help_text="Comma-separated keywords for search engines.")
    seo_description = models.TextField('seo_description', blank=True)

    #extra_fields = ExtraFieldsDefinition(null=True, blank=True)

    class Meta:
        verbose_name = _('category')
        verbose_name_plural = _('categories')

    def get_excerpt(self):
        if not self.description:
            return ''
        if len(self.description) > 65:
            return self.description[:65] + '...'
        else:
            return self.description
    get_excerpt.short_description = 'Description'
    
    def __unicode__(self):
        return self.name
    
    #def get_extra_form_fields(self):
    #    fields = self.get_extra_model_fields();
    #    ret = {}
    #    for key, field in fields.items():
    #        ret[key] = field.formfield()
    #    return ret
    
    #get_extra_model_fields
    
    @models.permalink
    def get_absolute_url(self):
        return ('cadoshop.views.search', (), {'category_slug': self.slug})

class Manufacturer(Sluggable):
    class Meta:
        verbose_name = 'Brand'
        verbose_name_plural = 'Brands'
    
    name = models.CharField(_('name'), max_length=256)
    logo = ProcessedImageField([ResizeToFit(220, 220, mat_color=(255, 255, 255))], upload_to='manufacturers', format='JPEG', options={'quality': 90}, blank=True)
    tiny_thumbnail = ImageSpecField([ResizeToFit(50, 50, mat_color=(255, 255, 255))],
                               source='logo',
                               format='JPEG', options={'quality': 90})
    url = models.URLField(verbose_name = u'URL')
    def __unicode__(self):
        return self.name

class Supplier(Sluggable):
    name = models.CharField(_('name'), max_length=256)
    def __unicode__(self):
        return self.name    
    
from django.core.exceptions import ValidationError


class Product(PriceBase, ExtraFieldsUser, Sluggable):
    
    PROVIDER_FIELD = 'category'
    
    category = TreeForeignKey(ProductCategory)
    manufacturer = models.ForeignKey(Manufacturer, verbose_name = 'Brand', blank=True, null=True)
    supplier = models.ForeignKey(Supplier, blank=True, null=True)
    
    fits_to = models.ManyToManyField('Product', related_name='fits', blank=True, null=True)
    
    is_active = models.BooleanField(_('is active'), default=True)
    name = models.CharField(_('name'), max_length=100)
    code = models.CharField(_('part number'), max_length=100, blank=True, null= True)
    #slug = models.SlugField(_('slug'), unique=True)
    #ordering = models.PositiveIntegerField(_('ordering'), default=0)
        
    #extra = ExtraFieldsValues(null=True, blank=True)
    colors = ColorsField(blank = True)

    image1 = models.ImageField(verbose_name = _('Image 1'), upload_to='products', blank=True)
    image2 = models.ImageField(verbose_name = _('Image 2'), upload_to='products', blank=True)
    image3 = models.ImageField(verbose_name = _('Image 3'), upload_to='products', blank=True)
    image4 = models.ImageField(verbose_name = _('Image 4'), upload_to='products', blank=True)
    image5 = models.ImageField(verbose_name = _('Image 5'), upload_to='products', blank=True)
    thumbnail = ImageSpecField([ResizeToFit(170, 170, mat_color=(255, 255, 255))],
                               source='image1',
                               format='JPEG', options={'quality': 90})
    tiny_thumbnail = ImageSpecField([ResizeToFit(50, 50, mat_color=(255, 255, 255))],
                               source='image1',
                               format='JPEG', options={'quality': 90})
    
    description = HTMLField(_('description'), blank=True)
    forced_order = models.FloatField(_('forced search weight'), default=10.0)
    

    class Meta:
        #ordering = ['ordering', 'name']
        verbose_name = _('product')
        verbose_name_plural = _('products')
  
    #def __init__(self, *args, **kwargs):
    #    self._meta.get_field('_unit_price').decimal_places = 2
    #    super(Product, self).__init__(*args, **kwargs)
              
    def __unicode__(self):
        return self.name

    def get_price_string(self):
        if self.category.product_default_count > 1:
            return u'%s %.2f/%d%s' % (self.currency, self.unit_price * self.category.product_default_count, self.category.product_default_count, self.category.product_unit)
        return u'%s %.2f' % (self.currency, self.unit_price)
    get_price_string.short_description = "Price"

    def get_price_string_excl_tax(self):
        return u'%.2f %s' % (self.unit_price_excl_tax, self.currency)

    def get_price_string_withtaxinfo(self):
        return u'%.2f %s (%.2f %s z VAT)' % (self.unit_price_excl_tax, self.currency, self.unit_price ,self.currency)

    @models.permalink
    def get_absolute_url(self):
        return ('cadoshop.views.details', (), {'product_slug': self.slug})
    
    def get_full_absolute_url(self):
        return settings.CADO_FULL_URL + self.get_absolute_url()
    
    def get_admin_edit_url(self):
        return '/admin/cadoshop/product/%d/' % self.id

    def get_bc_path(self):
        ret = []
        path = self.category.get_ancestors(include_self=True)
        for category in path:
            ret.append((reverse('cadoshop.views.search', kwargs={'category_slug':category.slug}), category.name))
        ret.append(('', self.name,))
        return ret

    def clean(self, *args, **kwargs):
        if self.code == "":
            self.code = None;
        
        if self.code:
            queryset = Product.objects.filter(code=self.code);
            if self.id:
                 queryset = queryset.exclude(id=self.id);
            exist = queryset.count()
            
            if exist != 0:
                raise ValidationError("Product with code %s already exists" % self.code)
        
        return super(Product, self).clean(*args, **kwargs)

class ProductAttachment(models.Model):
    product = models.ForeignKey(Product, related_name='attachments')
    name = models.CharField(max_length=256)
    file = models.FileField(upload_to='attachments')

from django.utils.safestring import mark_safe
        
class ProductOption(ProductBase): #, ExtraFieldsUser):
    
    PROVIDER_FIELD = 'product.category'
    EXTRA_PARENT = 'product'
    
    items_in_stock = models.IntegerField(default=0)
    
    product = models.ForeignKey(Product, related_name='options')
    price_mod = models.IntegerField(null=True, blank=True)
    name = models.CharField(_('name'), max_length=100, blank=True, null=True)
    #extra = ExtraFieldsValues(null=True, blank=True)
    colors = ColorsField(blank=True, null=True)

    image = models.ImageField(verbose_name = _('Image'), upload_to='products', blank=True)
    
    thumbnail = ImageSpecField([ResizeToFit(170, 170, mat_color=(255, 255, 255))],
                               source='image',
                               format='JPEG', options={'quality': 90})
    tiny_thumbnail = ImageSpecField([ResizeToFit(50, 50, mat_color=(255, 255, 255))],
                               source='image',
                               format='JPEG', options={'quality': 90})
    def __unicode__(self):
        return '%s (%s)' % (self.product.name, self.name)

    def show_left(self):
        if settings.CADOSHOP_MANAGESTOCK:
            return 'in stock: %d' % self.items_in_stock
        return ''
    
    def get_price_string(self):
        if self.product.category.product_default_count > 1:
            return u'%s %.2f/%d%s' % (self.product.currency, self.product.unit_price*self.product.category.product_default_count, self.product.category.product_default_count, self.product.category.product_unit)
        return u'%s %.2f' % (self.product.currency, self.product.unit_price)
    
    #<td>{{ orderitemform.instance.unit_price|floatformat:2 }}</td>


    def handle_order_item(self, orderitem):
        ProductBase.handle_order_item(self, orderitem)
        PriceBase.handle_order_item(self.product, orderitem)

    def get_price(self, *args, **kwargs):
        return self.product
    
    def get_thumbnail_img(self):
        if self.image:
            return mark_safe('<img src="%s"/>'% self.thumbnail.url)
        return mark_safe('<img src="%s"/>'% self.product.thumbnail.url)
    
    def get_tiny_thumbnail_path(self):
        if self.image:
            return self.tiny_thumbnail.path
        elif self.product.tiny_thumbnail:
            return self.product.tiny_thumbnail.path
        else:
            return settings.STATIC_ROOT + 'img/no_photo_tiny_thumb.png'

    def get_tiny_thumbnail_img(self):
        if self.image:
            return mark_safe('<img src="%s"/>'% self.tiny_thumbnail.url)
        elif self.product.tiny_thumbnail:
            return mark_safe('<img src="%s"/>' % self.product.tiny_thumbnail.url)
        else:
            return mark_safe('<img src="%simg/no_photo_tiny_thumb.png"/>' % settings.STATIC_URL)


def update_items_in_stock(instance, **kwargs):
    from plata.product.stock.models import StockTransaction, Period

    period,created = Period.objects.get_or_create(name='New period')
    instance.stock_transactions.create(
                period=period,
                type=StockTransaction.INITIAL,
                change=instance.items_in_stock,
                notes='Item form update',
                )

if settings.CADOSHOP_MANAGESTOCK:
    from django.db.models import signals
    signals.post_save.connect(update_items_in_stock, sender=ProductOption)
    
    
    """
    def __init__(self, *args, **kwargs):
        super(ProductOption, self).__init__(*args, **kwargs)
        
        self.extra_fields = {}
        try:
            for key, field in self.product.category.get_extra_model_fields().items():
                try:
                    self.extra_fields[key] = field.to_python(self.extra[key])
                    if not self.extra_fields[key]:
                        self.extra_fields[key] = field.to_python(self.product.extra[key])
                        
                except Exception:
                    try:
                        self.extra_fields[key] = field.to_python(self.product.extra[key])
                    except Exception:
                        self.extra_fields[key] = field.get_default();
        except Exception:
            pass
    """
    
class ContactUs(models.Model):
    class Meta:
        verbose_name = 'Contact Message'
        verbose_name_plural = 'Contact Messages'
    name = models.CharField(_('Name'), max_length=256)
    email = models.EmailField(_('Email'))
    message = models.TextField(_('Message'))
    def __unicode__(self):
        return "%s <%s>" % (self.name, self.email)
    

from django.template.loader import render_to_string
from django.core.mail import EmailMessage

class EmailHandler(object):
    def __init__(self, type):
        self.type = type

    def __call__(self, sender, **kwargs):
        if self.type == "confirmed":
            subject = "%s Order %s Confirmed" % (settings.CADO_NAME, kwargs['order'])
        else:
            subject = "%s Order %s Paid" % (settings.CADO_NAME, kwargs['order'])
             
        email = EmailMessage(subject=subject, body=render_to_string('email/order_%s.html' % self.type, kwargs))
        email.from_email = settings.FROM_EMAIL
        email.to += [kwargs['order'].email]
        email.to += settings.SHOP_EMAIL_TO
        email.bcc += settings.SHOP_EMAIL_BCC
        email.content_subtype = "html"
        email.send()
        
from plata.shop import notifications, signals as shop_signals

#shop_signals.contact_created.connect(EmailHandler("Contact Created"),weak=False)
shop_signals.order_confirmed.connect(EmailHandler("confirmed"), weak=False)
shop_signals.order_paid.connect(EmailHandler("paid"), weak=False)

       