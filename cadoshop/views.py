from django import forms
from django.forms import fields
from django.contrib import messages
from django.core.exceptions import ValidationError
from django.db.models import ObjectDoesNotExist
from django.shortcuts import get_object_or_404, redirect, render, render_to_response
from django.template import loader, Context, RequestContext
from django.utils.translation import ugettext as _
from django.core.paginator import Paginator, InvalidPage
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.mail import send_mail

import urllib

from plata.contact.models import Contact
from plata.discount.models import Discount
from plata.shop.views import Shop
from plata.shop.models import Order
from django.forms.forms import Form
from django.http import HttpResponse

from models import Product, ProductCategory, ProductOption, ContactUs, Manufacturer, StaticPage
from cadocms.models import User

from haystack.query import SearchQuerySet
from django.conf import settings

if settings.HOST.CLASS == 'DEV':
    from search.dummy_grouping_backend import GroupedSearchQuerySet
else:
    from search.solr_grouping_backend import GroupedSearchQuerySet

from haystack.inputs import AutoQuery
from django.conf import settings
from django.forms import ModelForm
from django.contrib import messages

# import the logging library
import logging
logger = logging.getLogger(__name__)

def frontend_context(request): 
    context = {}
    order = shop.order_from_request(request)
    context['user_order'] = order
    context['user_contact'] = shop.contact_from_user(request.user)
    context['user_order_total'] = order.items.count() if order else 0
    """
    all_facets = SearchQuerySet().facet('category').facet('tags').facet_counts().get('fields', {})
    
    context['tags'] = {}
    for tag, count in all_facets.get('tags', {}):
        context['tags'][tag] = {'total':count, 'count':count}
    categories_map = {}
    for category, count in all_facets.get('category', {}):
        categories_map[category] = count
    """
    context['categories'] = ProductCategory.objects.all()
    context['manufacturers'] = Manufacturer.objects.all()
    """
    categories = ProductCategory.objects.all()
    context['categories'] = [];
    for category in categories:
        category.total = categories_map.get(category.slug, 0)
        category.count = category.total
        context['categories'].append(category)
    """
    
    context['search_params'] = {};
    
    #print request
    #print context['facet']
    return context

from plata.contact.forms import CheckoutForm
from plata.shop.models import Order

class CadoCheckoutForm(CheckoutForm):
    shipping_pickup = forms.BooleanField(label="I will pick up my order in store", required=False)
    
    def clean(self):
        data = super(CheckoutForm, self).clean()

        if not data.get('shipping_pickup') and not data.get('shipping_same_as_billing'):
            for f in self.REQUIRED_ADDRESS_FIELDS:
                field = 'shipping_%s' % f
                if not data.get(field):
                    self._errors[field] = self.error_class([
                        _('This field is required.')])
                    
        if data.get('shipping_pickup'):
            for f in Order.ADDRESS_FIELDS:
                field = 'shipping_%s' % f
                data[field] = ''
            data['shipping_address']='Pickup in shop'

        return data

class CadoShop(Shop):
    def checkout_form(self, request, order):
        """Returns the address form used in the first checkout step"""

        # Only import plata.contact if necessary and if this method isn't
        # overridden
        return CadoCheckoutForm

shop = CadoShop(Contact, Order, Discount)

def extrafields(request, category_id):
    category = ProductCategory.objects.get(id=category_id)
    form = Form()
    for key, field in category.get_extra_fields():
        form.fields['extra[%s]' % key] = field['field'].formfield()
    return HttpResponse(form.as_p())


from django.core.mail import EmailMessage

class ContactUsForm(ModelForm):
    class Meta:
        model = ContactUs
        
    def save(self):
        super(ContactUsForm, self).save()
        EmailMessage(settings.CADO_NAME + ' [CONTACT FORM]', 
                     self.data['message'], 
                     settings.FROM_EMAIL, 
                     settings.SHOP_EMAIL_TO,
                     settings.SHOP_EMAIL_BCC,
                     headers = {'Reply-To': self.data['email']}).send()
        
def contact(request):
    context = {}
    context_instance = RequestContext(request)
    
    if request.method == 'POST':
        context['form'] = ContactUsForm(request.POST)
        if context['form'].is_valid():
            context['form'].save()
            messages.success(request, 'Your message was sent to us. We will contact you back as soon as possible.')
            context['form'] = 'Thank You!'
            # do something.
    else:
        context['form'] = ContactUsForm()
        
    return render_to_response('shop/contact.html', context, context_instance)

def index(request):    
    context = {}
    context_instance = RequestContext(request)
    if settings.INDEX_STATIC_PAGE:
        staticpage = get_object_or_404(StaticPage, url=settings.INDEX_STATIC_PAGE)
        context = RequestContext(request, {'staticpage': staticpage})
        return HttpResponse(loader.get_template('staticpage.html').render(context))
    context['suggested'] = Product.objects.order_by('?')[:5]
    return render_to_response('shop/index.html', context, context_instance)

def search(request, category_slug='all'):

    logger.info('Search View')
    context = {}
    context_instance = RequestContext(request)
    
    
    if request.method == 'POST':
        order = shop.order_from_request(request, create=True)
        try:
            option = request.POST.get('option', None)
            print option
            order.modify_item(ProductOption.objects.get(id=request.POST.get('product')), int(request.POST.get('count')))
            messages.success(request, _('The cart has been updated.'))
        except ValidationError, e:
            if e.code == 'order_sealed':
                [messages.error(request, msg) for msg in e.messages]
            else:
                raise
        return redirect(request.get_full_path())

    if ('category' in request.GET):
        newGET = request.GET.copy()
        newGET.pop('category', None)
        category_slug = request.GET['category']
        return HttpResponseRedirect(reverse('cadoshop.views.search', kwargs={'category_slug':category_slug})
                                            + '?' + urllib.urlencode(newGET));


    results_per_page = 21
    results = GroupedSearchQuerySet().group_by('product').facet('category', limit=500)
    
    context['search_params'] = {}
    context['breadcrumbs'] = [] #(reverse('cadoshop.views.search', kwargs={'category_slug':'all'}), 'All Products')] 
    context['categories_path'] = []
    
    available_filters = [u'price', u'has_image', u'manufacturer']
    #context['search_params'] = dict((key, request.GET.get(key, None)) for key in ['q', 'page', 'tags'] )

    if category_slug:
        #results = results.filter(category=category_slug)
        context['category'] = category = get_object_or_404(ProductCategory, slug=category_slug)
        for key, field in category.get_extra_fields():
            available_filters.append(field['solr_key'])
        
        path = category.get_ancestors(include_self=True)
        for category in path:
            context['categories_path'].append(category.slug);
            context['breadcrumbs'].append((reverse('cadoshop.views.search', kwargs={'category_slug':category.slug}), category.name))
    else:
        return HttpResponseRedirect(reverse('cadoshop.views.search', kwargs={'category_slug':'all'}));

    #context['breadcrumbs']

    if 'q' in request.GET and request.GET['q']:
        context['search_params']['q'] = request.GET['q']
        results = results.filter(text=AutoQuery(request.GET['q']))
        
    if 'view' in request.GET and request.GET['view']:
        context['search_params']['view'] = 'list'

    if 'order' in request.GET and request.GET['order']:
        context['search_params']['order'] = request.GET['order']
        results = results.order_by('-forced_order', context['search_params']['order'], 'score')
    else:
        results = results.order_by('-forced_order', 'score')

        
    if 'tags' in request.GET and request.GET['tags']:
        context['search_params']['tags'] = request.GET['tags']
        tags = request.GET['tags'].split(',')
        for tag in tags:
            results = results.filter(tags=tag)
    
    for key in available_filters:
        if 'filter[' + key +']' in request.GET and request.GET['filter[' + key +']']:
            vvv = request.GET['filter[' + key +']']
            if key != ('manufacturer'):
                vvv = vvv.replace(' ', '_')
            context['search_params']['filter[' + key +']'] = vvv
            results = results.filter(**{key:vvv})
        if 'filter[' + key +'_from]' in request.GET and request.GET['filter[' + key +'_from]']:
            context['search_params']['filter[' + key +'_from]'] = request.GET['filter[' + key +'_from]']
            results = results.filter(**{key + '__gte':request.GET['filter[' + key +'_from]']})
        if 'filter[' + key +'_to]' in request.GET and request.GET['filter[' + key +'_to]']:
            context['search_params']['filter[' + key +'_to]'] = request.GET['filter[' + key +'_to]']
            results = results.filter(**{key + '__lte':request.GET['filter[' + key +'_to]']})
    
    #print 'Q1 %s' % results
    context['facet'] = results.facet_counts().get('fields', {})
    import json
    context['facet_json'] = json.dumps(context['facet'])

    if category_slug:
        results = results.filter(category_exact=category_slug)
    
    #print 'Q2 %s' % results
    
    #print context['search_params']
    if context['search_params']:
        urlparams = urllib.urlencode(dict([k, v.encode('utf-8')] for k, v in context['search_params'].items()))
        print context['search_params']
        context['breadcrumbs'].append((
            reverse('cadoshop.views.search', kwargs={'category_slug':category_slug}) + '?' + urlparams
            , 'Search Results'))
    
    #results = results.filter_and(price=12)
    #print request.GET
    
    
    
    categories_map = {}
    for category, count in context['facet'].get('category', {}):
        categories_map[category] = count

    new_categories = [];
    for category in context_instance['categories']:
        category.count = categories_map.get(category.slug, 0)
        new_categories.append(category)
        
    context_instance['categories'] = new_categories

    
    #results = results.load_all()
    try:
        page_no = int(request.GET.get('page', 1))
        if (page_no > 1):
            context['breadcrumbs'].append(('', 'Page %d' % page_no))
            
    except (TypeError, ValueError):
        raise Http404("Not a valid number for page.")

    if page_no < 1:
        raise Http404("Pages should be 1 or greater.")

    context['search_params']['page'] = page_no
    #context['search_params']['search'] = True
    context['search_params']['category'] = category_slug

    if not 'order' in context['search_params']:
        context['search_params']['order'] = 'score'

    context['breadcrumbs'][-1] = ('',) + context['breadcrumbs'][-1][1:]


    start_offset = (page_no - 1) * results_per_page
    #results[start_offset:start_offset + results_per_page]

    paginator = Paginator(results, results_per_page)

    try:
        page = paginator.page(page_no)
    except InvalidPage:
        raise Http404("No such page!")

    #for result in results:
    #    extra = []
    #    for key,item in result._object.category.extra_fields.items():
    #        extra.append({'label':key, 'value':result._object.extra.get(key, 'ASD') }) 
    #    result.extra = extra
    
    context['results'] = results
    context['page'] = page
    context['search'] = request.GET
    
    context['productpage'] = settings.CADOSHOP_PRODUCTPAGE
    
    
    #print page
    #Product.objects.filter(is_active=True)
    #print 'RENDER', context['search_params']
    return render_to_response('shop/search.html', context, context_instance)
    

def details(request, product_slug):
    product = get_object_or_404(Product.objects.filter(is_active=True), slug=product_slug)

    if request.method == 'POST':
        order = shop.order_from_request(request, create=True)
        try:
            order.modify_item(ProductOption.objects.get(id=request.POST.get('product')), int(request.POST.get('count')))
            messages.success(request, _('The cart has been updated.'))
        except ValidationError, e:
            if e.code == 'order_sealed':
                [messages.error(request, msg) for msg in e.messages]
            else:
                raise

        return redirect('plata_shop_cart')
    
    context = {};
    context['object'] = product
    context['options'] = product.options.all()
    
    context['breadcrumbs'] = []
    path = product.category.get_ancestors(include_self=True)
    for category in path:
        context['breadcrumbs'].append((reverse('cadoshop.views.search', kwargs={'category_slug':category.slug}), category.name))

    context['breadcrumbs'].append(('', product.name,))

    context['fits_to'] = product.fits_to.all()
    context['fits'] = product.fits.all()
    
    return render_to_response('shop/details.html', context, context_instance=RequestContext(request))



from django.contrib.auth import authenticate, login, logout

def validate_password(password):
    if len(password) < 8:
        raise forms.ValidationError("Password must be at least 8 characters long.")
    # At least one letter and one non-letter
    first_isalpha = password[0].isalpha()
    if all(c.isalpha() == first_isalpha for c in password):
        raise forms.ValidationError("Password must contain at least one letter and at least one digit or" \
                                    " punctuation character.")


class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs=dict(required=True, max_length=30)))
    password = forms.CharField(widget=forms.PasswordInput(attrs=dict(required=True, max_length=30, render_value=False)))
    
    def clean(self):
        if ('username' in self.cleaned_data) and ('password' in self.cleaned_data): 
            self.user = authenticate(username=self.cleaned_data['username'], password=self.cleaned_data['password'])
            if self.user is None:
                raise forms.ValidationError("Wrong username or password")
            if not self.user.is_active:
                raise forms.ValidationError("This account is disabled")
        return self.cleaned_data
    

def view_login(request):
    context = {};
    context['form'] = form = LoginForm(request.POST if request.method == 'POST' else None)
    if(request.method == 'POST') and form.is_valid():
        login(request, form.user)
        return redirect(request.GET.get('next','/'))
    return render_to_response('login.html', context, context_instance=RequestContext(request))

def view_logout(request):
    logout(request)
    return redirect('/login/') 

from django.contrib.auth.forms import UserCreationForm

def view_register(request):
    
    form = UserCreationForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            new_user = form.save()
            #messages.info(request, "Thanks for registering. You are now logged in.")
            new_user = authenticate(username=form.cleaned_data['username'],
                                    password=form.cleaned_data['password1'])
            login(request, new_user)
            return redirect(request.GET.get('next','/'))
        
    return render(request, "register.html", {'form': form,})

