from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from cadoshop.models import Product
from cadoshop.models import ProductOption

class Command(BaseCommand):
    def handle(self, *args, **options):
        
        for product in Product.objects.all():
            if product.options.all().count() == 0:
                product.options.add(ProductOption(name='default'))
            
            print product.options.all().count()        
        print 'DONE'
