import urllib, re

from django import template
from django.utils.safestring import mark_safe
from django.template import loader, RequestContext
from django.core.urlresolvers import reverse
from django.forms.forms import Form
from django.forms import fields
from django.db import models
from django.conf import settings

register = template.Library()

from cadocms.models import MyTextMultiField
from cadoshop.models import ProductCategory, Manufacturer, Product,\
    ProductOption
    
from haystack import indexes

@register.simple_tag(name="search_url", takes_context=True)
def get_search_url(context, category=None, tag=None, page=1, view=None, order=None):
    
    urlparams = context['search_params'].copy()
    if 'category' in urlparams:
        del urlparams['category']
        
    category_slug = category if category is not None else context['search_params'].get('category', '')
    urlparams['tags'] = tag if tag is not None else context['search_params'].get('tags', None)
    urlparams['page'] = page if page is not None else context['search_params'].get('page', None)
    
    urlparams['view'] = view if view is not None else context['search_params'].get('view', None)
    urlparams['order'] = order if order is not None else context['search_params'].get('order', None)

    urlparams = dict([k, str(v).encode('utf-8')] for k,v in urlparams.iteritems() if v is not None)    
    if (urlparams['page'] == '1'):
        del urlparams['page']
    
    if (urlparams.get('view', None) == 'grid'):
        del urlparams['view']

    if (urlparams.get('order', None) == 'score'):
        del urlparams['order']
    
    return reverse('cadoshop.views.search', kwargs={'category_slug':category_slug}) + '?' + urllib.urlencode(urlparams)

@register.filter
def urlize_hashtags(value):
    def repl(m):
        hashtag = m.group(1)
        url = reverse('cadoshop.views.search', kwargs={'category_slug':'all'}) + '?tags=' + hashtag
        return '<a href="%s">&#35;%s</a>' % (url, hashtag)
    hashtag_pattern = re.compile(r'[#]+([-_a-zA-Z0-9]+)')
    return mark_safe(hashtag_pattern.sub(repl, value))

import string
@register.filter
def printable(value):
    return filter(lambda x: x in string.printable, value)

def get_search_filter_form(context):
    
    form = Form(initial=context['search_params'])
    form.fields['q'] = fields.CharField(label = 'contains query')
    
        
    #form.fields['color'] = fields.IntegerField()
    return form

@register.simple_tag(name="search_form_full", takes_context=True)
def search_form_full(context):
    return loader.render_to_string('shop/search_form_full.html', {'form' : get_search_filter_form(context)}, context_instance=RequestContext(context['request']));

@register.simple_tag(name="search_form_simple", takes_context=True)
def search_form_simple(context):
    
    form = Form(initial=context['search_params'])
    form.fields['q'] = fields.CharField(label = 'search')
    
    category_slug = context['search_params'].get('category', 'all')
    #print category_slug, 'ASD' 
    category_path = ProductCategory.objects.get(slug=category_slug).get_ancestors(include_self=True)
    """
    options = []
    for category in category_path:
        options.append((category.slug, 'in ' + category.name))
    if len(options) > 1:
        form.fields['category'] = fields.ChoiceField(label='', choices=options)
    #form.fields['color'] = fields.IntegerField()
    """
    return loader.render_to_string('shop/search_form_simple.html', 
                                   {'form' : form,
                                    'category_path' : category_path
                                    },
                                   context_instance=RequestContext(context['request']));
                                   
@register.filter
def display_extra_fields(value):
    #{% for key, item in object.extra_fields.items %}
    #{{key}}: &nbsp; <strong>{{item|join:", " }}</strong><br/>
    #{% endfor %}
    
    ret = ""
    if type(value) is Product:
        category = value.category
    elif type(value) is ProductOption:
        category = value.product.category
    else:
        return "ERROR"
    
    #ret.append((key, {'field' : f, 'solr_key' : solr_key, 'params' : field.get('params', {}).copy()}))

    for key, field in category.get_extra_fields():
        ret = ret + field['field'].verbose_name + ' : <strong>'
        if type(field['field']) is MyTextMultiField:
            ret = ret + ", ".join(value.extra_fields.get(key,[]))
        else:
            ret = ret + unicode(value.extra_fields.get(key, ''))
        if 'suffix' in field['params']:
            ret = ret + field['params']['suffix']
            
        ret = ret + '</strong><br/>'
        
         
    return mark_safe(ret)
    
@register.filter
def display_important_extra_fields(value):
    #{% for key, item in object.extra_fields.items %}
    #{{key}}: &nbsp; <strong>{{item|join:", " }}</strong><br/>
    #{% endfor %}
    
    ret = ""
    if type(value) is Product:
        category = value.category
    elif type(value) is ProductOption:
        category = value.product.category
    else:
        return "ERROR"
    
    #ret.append((key, {'field' : f, 'solr_key' : solr_key, 'params' : field.get('params', {}).copy()}))

    for key, field in category.get_extra_fields():
        if 'important' in field['params']:
            ret = ret + field['field'].verbose_name + ' : <strong>'
            if type(field['field']) is MyTextMultiField:
                ret = ret + ", ".join(value.extra_fields[key])
            else:
                ret = ret + unicode(value.extra_fields[key])
            if 'suffix' in field['params']:
                ret = ret + field['params']['suffix']
                
            ret = ret + '</strong><br/>'
        
         
    return mark_safe(ret)
    
@register.simple_tag(name="search_form_advanced", takes_context=True)
def search_form_advanced(context):
    
    category_slug = context['search_params'].get('category', 'all')
    filters = []
    djangofields = []

    """
    filters['has_image'] = {'type': 'bool', 'label': 'Has Image', 
                            'value':context['search_params'].get('filter[has_image]')}
    """
    
    if settings.CADOSHOP_SEARCHABLEMANUFACTURERS:
        manufacturers = Manufacturer.objects.all()
        moptions = []
        for m in manufacturers:
            moptions.append((m.slug, m.name))
        filters.append(('manufacturer', {
                                   'type': 'select', 
                                   'label': 'Brand', 
                                   'choices': moptions,
                                   'value': context['search_params'].get('filter[manufacturer]')
                                   }))
    
    #print context['search_params'].get('filter[manufacturer]')
    
    if category_slug:
        category = ProductCategory.objects.get(slug=category_slug)
        category_path = category.get_ancestors(include_self=True)
        options = []
        for cat in category_path:
            options.append((cat.slug, cat.name))
        #if len(options) > 1:
        #    form.fields['category'] = fields.ChoiceField(label='category', choices=options)
        #form.fields['category'] = fields. (label='category')    
        for key, field in category.get_extra_fields():
            djangofields.append((field['solr_key'], field['field'])) 
    

    for key, field in djangofields:
        #print field.__class__.__name__
        if field._choices:
            filters.append((key, {'type': 'select', 'label': field.verbose_name, 'choices': field._choices,
                            'value': context['search_params'].get('filter['+key+']')
                            }))
        elif isinstance(field, models.IntegerField):
            filters.append((key, {'type': 'int', 'label': field.verbose_name,
                        'from' : context['search_params'].get('filter['+key+'_from]'),
                        'to' : context['search_params'].get('filter['+key+'_to]')}))
            #form.fields['filter[%s_from]' % key] = fields.IntegerField(label = field.verbose_name + ' from')
            #form.fields['filter[%s_to]' % key] = fields.IntegerField(label = field.verbose_name + ' to')
        elif isinstance(field, models.FloatField):
            filters.append((key, {'type': 'float', 'label': field.verbose_name,
                        'from' : context['search_params'].get('filter['+key+'_from]'),
                        'to' : context['search_params'].get('filter['+key+'_to]')}))
            #form.fields['filter[%s_from]' % key] = fields.FloatField(label = field.verbose_name + ' from')
            #form.fields['filter[%s_to]' % key] = fields.FloatField(label = field.verbose_name + ' to')
        elif isinstance(field, models.BooleanField):
            filters.append((key, {'type': 'bool', 'label': field.verbose_name, 'value': context['search_params'].get('filter['+key+']')}))
        else:
            filters.append((key, {'type': 'string', 'label': field.verbose_name, 'value': context['search_params'].get('filter['+key+']')}))
            #form.fields['filter[%s]' % key] = field.formfield()
    
    filters.append(('price', {'type': 'float',
                        'label': 'Price',
                        'unit' : '$',
                        'from' : context['search_params'].get('filter[price_from]'),
                        'to' : context['search_params'].get('filter[price_to]')
                        }))
    
    return loader.render_to_string('shop/search_form_advanced.html', {
                                                                      'category_slug':context['search_params'].get('category', 'all'),
                                                                      'form' : get_search_filter_form(context),
                                                                      'filters' : filters,
                                                                      }, context_instance=RequestContext(context['request']));
