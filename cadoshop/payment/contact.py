"""
Payment module for cash on delivery handling

Automatically completes every order passed.
"""

from django.shortcuts import redirect
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

import plata
from plata.payment.modules.base import ProcessorBase
from plata.shop.models import OrderPayment


class PaymentProcessor(ProcessorBase):
    key = 'contact'
    default_name = _('Please Contact Me')

    def process_order_confirmed(self, request, order):
        order = order.reload()

        if plata.settings.PLATA_STOCK_TRACKING:
            StockTransaction = plata.stock_model()
            self.create_transactions(order, _('sale'),
                type=StockTransaction.SALE, negative=True, payment=None)
        
        return self.shop.render(request, 'payment/contact_message.html', {
            'order': order,
        })