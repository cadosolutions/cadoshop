from django.shortcuts import redirect
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

import plata
from plata.payment.modules.base import ProcessorBase
from plata.shop.models import OrderPayment
from decimal import Decimal


class PaymentProcessor(ProcessorBase):
    key = 'contact'
    default_name = _('Cash')

    def process_order_confirmed(self, request, order):
        print 'CASH', order.__dict__
        order = order.reload()
        order.shipping_cost = Decimal('0.00')
        order.shipping_discount = Decimal('0.00')
        order.shipping_tax = Decimal('0.00')
        order = order.reload()
        order.save();
    
        if plata.settings.PLATA_STOCK_TRACKING:
            StockTransaction = plata.stock_model()
            self.create_transactions(order, _('sale'),
                type=StockTransaction.SALE, negative=True, payment=None)
        
        return self.shop.render(request, 'payment/cash_message.html', {
            'order': order,
        })