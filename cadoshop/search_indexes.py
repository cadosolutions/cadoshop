from haystack import indexes
from celery_haystack.indexes import CelerySearchIndex
from models import ProductOption
from django.db import models

import re

class ProductOptionIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    tags = indexes.MultiValueField(faceted=True)
    category = indexes.MultiValueField(faceted=True)
    
    manufacturer = indexes.CharField()
    
    product = indexes.IntegerField(model_attr='product__id')
    
    forced_order = indexes.FloatField()
    
    price = indexes.FloatField()
    name = indexes.CharField(boost=2.0, faceted=True)
    colors = indexes.MultiValueField()
    has_image = indexes.BooleanField()
    
    def get_model(self):
        return ProductOption
    
    def index_queryset(self, using=None):
        return self.get_model().objects.filter(product__is_active=True) #filter(pub_date__lte=datetime.datetime.now())
    
    def prepare_tags(self, obj):
        return re.findall(r'[#]+([-_a-zA-Z0-9]+)', obj.product.description)
    
    def prepare_category(self, obj):
        path = obj.product.category.get_ancestors(include_self=True)
        ret = []
        for category in path:
            ret.append(category.slug)
        return ret
    
    def prepare_price(self, obj):
        return obj.product._unit_price

    def prepare_name(self, obj):
        return obj.product.name
    
    def prepare_forced_order(self, obj):
        return obj.product.forced_order

    def prepare_colors(self, obj):
        return []
    
    def prepare_has_image(self, obj):
        return bool(obj.image or obj.product.image1)
    
    def prepare_manufacturer(self, obj):
        if obj.product.manufacturer is not None:
            return obj.product.manufacturer.slug

    def prepare(self, object):
        print "PREPARING PRODUCT %d %s" % (object.product.id, object.product)
        self.prepared_data = super(ProductOptionIndex, self).prepare(object)
        #print object.extra
        for key, field in object.product.category.get_extra_fields():
            value = object.product.extra_fields.get(key, None)   
            if isinstance(value, str) or isinstance(value, unicode):
                value = value.replace(' ', '_')
            if isinstance(value, list):
                #print value
                value = [v.replace(' ', '_') if isinstance(v, str) or isinstance(v, unicode) else v for v in value]
                #print value
            self.prepared_data[field['solr_key']] = value
            
            #<dynamicField name="*_l"  type="long" />
            #<dynamicField name="*_t"  type="text_en" />
            #<dynamicField name="*_f"  type="float" />
            #<dynamicField name="*_d"  type="double" />
            #<dynamicField name="*_p" type="location" />
        #print self.prepared_data
        return self.prepared_data

"""
def reindex_product(sender, **kwargs):
    ProductOptionIndex().update_object(kwargs['instance'])
    
models.signals.post_save.connect(reindex_product, sender=ProductOption)
"""
