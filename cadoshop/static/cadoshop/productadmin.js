(function($){
    $(document).ready(function($) {

		if ($('#options-group').length)
		{
			$.fn.grp_inline_original = $.fn.grp_inline; 
			$.fn.grp_inline_original.defaults = $.fn.grp_inline.defaults; 
			
			$.fn.grp_inline = function(opts){
                opts['onAfterAdded'] = function(form) {
                    document.registerExtraFieldsQueue();
                };

				ret = this.grp_inline_original(opts);
				if (opts.prefix == 'options')
				{
			    	//alert("asd");
			    	//$('#productoption_set-group')
			    	if ($('#options0').length == 0)
					{
						addButton = $('#options-group').find("a.grp-add-handler:last");
						addButton.click();
						$('#id_options-0-name').val('default');
						$('#options0 .grp-remove-handler').hide();
					}
			    	else
			    	{
			    		$('#options0 .grp-delete-handler').hide();
			    	}
			    	
				}
				return ret;
			}
			$.fn.grp_inline.defaults = $.fn.grp_inline_original.defaults;
		}
    });
})(grp.jQuery);