$(function(){

	//$("select").fixSelectStyles();
	
	$(".fancybox").fancybox({});
	
	$("select.optionSelect").change(function(){
		$(this).parents('.item').find('.optionPer').hide();
		$(this).parents('.item').find('.option' + $(this).val()).show();
	});
	$("select.optionSelect").change();
	
	if($("#tagcloud").length){
		$("#tagcloud").jQCloud(document.tags_list);
	}
	
	$('#suggestedCarousel').carousel({
		interval:3000
	})
})