# -*- coding: utf-8 -*-

from setuptools import setup

setup(
    name='django-cadoshop',
    version='0.1.0',
    description='Cado Shop',
    author='Frank Wawrzak (CadoSolutions)',
    author_email='frank.wawrzak@cadosolutions.com',
    url='https://github.com/fsw/django-cadoshop',
    download_url='git://github.com/fsw/django-cadoshop.git',
    packages=['cadoshop', 'plata'],
    install_requires=[
	#'plata',
        'xlwt',
    ],
    dependency_links = [
        #'https://bitbucket.org/fsw_/django-versioning/get/master.tar.gz#egg=fsw-django-versioning-0.7.3',
        #'http://github.com/fsw/plata/tarball/master#egg=fsw-plata-1.1.0',
    ],
)
